﻿using API_System.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace API_System.BLL
{
    public class UserService : IUserService
    {
        private readonly IHttpContextAccessor accessor;
       
        private API_SystemContext _context;
        public UserService(API_SystemContext Context, IHttpContextAccessor accessor)
        {
            this.accessor = accessor;
            this._context = Context;
        }

        //public string GetFullName()
        //{
        //    var user = accessor?.HttpContext?.User.Identity.Name;
        //    string userid = _context.AspNetUsers.FirstOrDefault(o => o.UserName == user).Id;
        //    return userid;
        //    //return _context.AEmployees.FirstOrDefault(o => o.UserId == userid).Name;
        //}

        public string GetFullName()
        {
            var user = accessor?.HttpContext?.User.Identity.Name;
            string userid = _context.AspNetUsers.FirstOrDefault(o => o.UserName == user).UserName;
            return userid;
            //return _context.AEmployees.FirstOrDefault(o => o.UserId == userid).Name;
        }

        public string GetFullNameID()
        {
            var user = accessor?.HttpContext?.User.Identity.Name;
            string userid = _context.AspNetUsers.FirstOrDefault(o => o.UserName == user).Id;
            return userid;
        }
        public ClaimsPrincipal GetUser()
        {
            return accessor?.HttpContext?.User as ClaimsPrincipal;
        }

        public string GetUserName()
        {
          return accessor?.HttpContext?.User.Identity.Name;
        }

        public int logins_Today()
        {
            var _logins = _context.ALoginLog.ToList();
            var logins = _logins.Where(o => Convert.ToDateTime(o.LoginDate).Date == DateTime.Now.Date).ToList().Count;
            return logins;
        }
    }
}
