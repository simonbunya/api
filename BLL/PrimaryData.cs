﻿using API_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_System.BLL
{
  
    public class SucessLoginCredentials
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string UserRole { get; set; }
        public int EmployeeId { get; set; }
        public string AccessToken { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string assignedfacilities { get; set; }
        public string assignedModules { get; set; }
    }
    public class LoginCredentials
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class UploadedData
    {
        public string AccessCode { get; set; }
        public List<Ona1Attachment> Ona1Attachment { get; set; }
        public List<Ona1Chilligrps> Ona1Chilligrps { get; set; }
        public List<Ona1Crc> Ona1Crc { get; set; }
        public List<Ona1Lgroups> Ona1Lgroups { get; set; }
        public List<Ona1McareGroups> Ona1McareGroups { get; set; }
        public List<Ona1samplegrps> Ona1samplegrps { get; set; }
       
    }
    
    public class ChangePassword
    {
        public string AccessCode { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}
