﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace API_System.BLL
{
    public interface IUserService
    {
        ClaimsPrincipal GetUser();
        string GetUserName();
        string GetFullNameID();
        string GetFullName();
        int logins_Today();
       

    }
}
