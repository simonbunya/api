﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Syncfusion.EJ2.Base;
using API_System.BLL;
using API_System.Models;

namespace API_System.Controllers
{
    public class UtilitiesController : Controller
    {
        private IWebHostEnvironment hostingEnv;
        private API_SystemContext context;
        private IUserService userInfor;

        public UtilitiesController(API_SystemContext Context, IWebHostEnvironment env, IUserService _userinfo)

        {
            this.hostingEnv = env;
            this.context = Context;
            this.userInfor = _userinfo;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region Roles

        public IActionResult Roles()
        {           

            return View();
        }
        public ActionResult RolesDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = context.AspNetRoles.AsNoTracking().OrderByDescending(o => o.Id).ToList();
            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public IActionResult AddRolePartial([FromBody] CRUDModel<AspNetRoles> value)
        {
           
            return PartialView("_DialogRoleAddPartial", value.Value);
        }
        public ActionResult EditRolePartial([FromBody] CRUDModel<AspNetRoles> value)
        {          

            return PartialView(value.Value);
        }

        public string CheckRoleId()
        {
            //string code = "0";
            string code = "awusgfgdf-ewqas-jkgr-zure-bncvxmkhipqw";
            int result = 0;
            result = context.AspNetRoles.ToList().Count();
            code += (result + 1);
            var x = new AspNetRoles()
            {
                Id = code
            };

            return (code);
        }

        public ActionResult DialogInsertRole([FromBody] CRUDModel<AspNetRoles> value)
        {
            string result = string.Empty;

            AspNetRoles _table = context.AspNetRoles.FirstOrDefault(o =>
            (o.Id == value.Value.Id));


            if (_table == null)
            {
                //Save into ASPNet Roles table
                AspNetRoles table = new AspNetRoles();

                table.Id = CheckRoleId();
                table.Name = value.Value.Name;
                table.NormalizedName = value.Value.Name.ToUpper();

                //table.DateAdded = DateTime.Now;
                //table.AddedBy = userInfor.GetUser().Identity.Name;

                context.AspNetRoles.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdateRoles(value);
            }
            return Json(value.Value);
        }

        public ActionResult DialogUpdateRoles([FromBody] CRUDModel<AspNetRoles> value)
        {
            AspNetRoles table = context.AspNetRoles.FirstOrDefault(o =>
             (o.Id == value.Value.Id));

            if (table != null)
            {
                table.Name = value.Value.Name;
                
                //table.DateEdited = DateTime.Now;
                //table.EditedBy = userInfor.GetUser().Identity.Name;

                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }



                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsertRole(value);
            }

            return Json(value.Value);
        }

        #endregion Roles

        #region Departments

        public IActionResult Departments()
        {
            return View();
        }
        public ActionResult DepartmentsDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = context.ADepartment.AsNoTracking().OrderByDescending(o => o.DepartmentId).ToList();
            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public IActionResult AddDepartmentPartial([FromBody] CRUDModel<ADepartment> value)
        {

            return PartialView("_DialogDepartmentAddPartial", value.Value);
        }
        public ActionResult EditDepartmentPartial([FromBody] CRUDModel<ADepartment> value)
        {

            return PartialView(value.Value);
        }

        public int CheckDepartmentCode()
        {
            var depcode = context.ADepartment.OrderByDescending(o => o.DepartmentId).Take(1).Select(f => f.DepartmentId).FirstOrDefault();
            return (depcode);
        }

        public ActionResult DialogInsertDepartment([FromBody] CRUDModel<ADepartment> value)
        {
            string result = string.Empty;

            ADepartment _table = context.ADepartment.FirstOrDefault(o =>
            (o.DepartmentId == value.Value.DepartmentId));


            if (_table == null)
            {                
                ADepartment table = new ADepartment();

                if (string.IsNullOrEmpty(table.DepartmentId.ToString()) || table.DepartmentId == 0)
                {
                    var newCode = CheckDepartmentCode();
                    table.DepartmentId = ++newCode;
                }

                table.Department = value.Value.Department;

                //table.DateAdded = DateTime.Now;
                //table.AddedBy = userInfor.GetUser().Identity.Name;

                context.ADepartment.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdateDepartment(value);
            }
            return Json(value.Value);
        }

        public ActionResult DialogUpdateDepartment([FromBody] CRUDModel<ADepartment> value)
        {
            ADepartment table = context.ADepartment.FirstOrDefault(o =>
             (o.DepartmentId == value.Value.DepartmentId));

            if (table != null)
            {
                table.Department = value.Value.Department;

                //table.DateEdited = DateTime.Now;
                //table.EditedBy = userInfor.GetUser().Identity.Name;

                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }



                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsertDepartment(value);
            }

            return Json(value.Value);
        }

        #endregion Departments

        #region Organization

        public IActionResult Organization()
        {
            return View();
        }
        public ActionResult OrganizationDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = context.AOrganization.AsNoTracking().OrderByDescending(o => o.OrganizationId).ToList();
            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public IActionResult AddOrganizationPartial([FromBody] CRUDModel<AOrganization> value)
        {

            return PartialView("_DialogOrganizationAddPartial", value.Value);
        }
        public ActionResult EditOrganizationPartial([FromBody] CRUDModel<AOrganization> value)
        {

            return PartialView(value.Value);
        }

        public int CheckOrganizationCode()
        {
            var orgcode = context.AOrganization.OrderByDescending(o => o.OrganizationId).Take(1).Select(f => f.OrganizationId).FirstOrDefault();
            return (orgcode);
        }

        public ActionResult DialogInsertOrganization([FromBody] CRUDModel<AOrganization> value)
        {
            string result = string.Empty;

            AOrganization _table = context.AOrganization.FirstOrDefault(o =>
            (o.OrganizationId == value.Value.OrganizationId));


            if (_table == null)
            {
                AOrganization table = new AOrganization();

                if (string.IsNullOrEmpty(table.OrganizationId.ToString()) || table.OrganizationId == 0)
                {
                    var newCode = CheckOrganizationCode();
                    table.OrganizationId = ++newCode;
                }

                table.Organization = value.Value.Organization;

                //table.DateAdded = DateTime.Now;
                //table.AddedBy = userInfor.GetUser().Identity.Name;

                context.AOrganization.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdateOrganization(value);
            }
            return Json(value.Value);
        }

        public ActionResult DialogUpdateOrganization([FromBody] CRUDModel<AOrganization> value)
        {
            AOrganization table = context.AOrganization.FirstOrDefault(o =>
             (o.OrganizationId == value.Value.OrganizationId));

            if (table != null)
            {
                table.Organization = value.Value.Organization;

                //table.DateEdited = DateTime.Now;
                //table.EditedBy = userInfor.GetUser().Identity.Name;

                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }



                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsertOrganization(value);
            }

            return Json(value.Value);
        }

        #endregion Organization
    }
}
