﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Xml.Linq;
using API_System.BLL;
using API_System.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Syncfusion.EJ2.Base;
using Syncfusion.EJ2.Navigations;
using System.Data.Entity.Validation;
using API_System.ModelView;

namespace API_System.Controllers
{
    public class AccountController : Controller
    {
      
        private API_SystemContext _context;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<AccountController> _logger;
        private readonly IEmailSender _emailSender;
        private IWebHostEnvironment hostingEnv;       
        private IUserService userInfor;

      
        public AccountController(API_SystemContext Context,
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            ILogger<AccountController> logger,
            IEmailSender emailSender,
            IWebHostEnvironment env,
            IUserService _userinfo)
        {
            this._context = Context;
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
           this. hostingEnv = env;
            this.userInfor = _userinfo;

        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            ResetPasswordViewModel m = new ResetPasswordViewModel();
            var userName = userInfor.GetUser().Identity.Name;
            var user = _context.AspNetUsers.FirstOrDefault(o => o.UserName == userName);
            m.Id = user.Id;
            m.Email = user.Email;
            return View(m);
        }
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(model.Id);
                var response1 = await _userManager.RemovePasswordAsync(user);
                if (response1.Succeeded)
                {
                    var response2 = await _userManager.AddPasswordAsync(user, model.Password);
                    if (response2.Succeeded)
                    {
                        await _signInManager.SignOutAsync();
                        return RedirectToAction("Login", "Account");
                    }
                    else
                    {
                        ModelState.AddModelError("", response2.Errors.ToString());
                    }
                }
                else
                {
                    ModelState.AddModelError("", response1.Errors.ToString());
                }
            }
            ResetPasswordViewModel m = new ResetPasswordViewModel();
            var userName = userInfor.GetUser().Identity.Name;
            var _user = _context.AspNetUsers.FirstOrDefault(o => o.UserName == userName);
            m.Id = _user.Id;
            m.Email = _user.Email;
            return View(m);
        }
        public IActionResult LoginManagement()
        {

            return View();
        }
        public ActionResult DataSourceLoginLogs([FromBody]DataManagerRequest dm)
        {
            var _data = _context.ALoginLog.OrderByDescending(o => o.LoginDate).ToList();
            IEnumerable data = _data;
            int _count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                _count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                _count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = _count });
        }

        public IActionResult PasswordManagement()
        {
            ViewBag.promptbutton = new
            {
                content = "Reset password",
                isPrimary = true
            };
            ViewBag.promptbutton1 = new
            {
                content = "Cancel",
            };
            return View();
        }
        public async Task<IActionResult> ResetUserPassword(string Id, string password)
        {
            string result = string.Empty;
            try
            {
                var user = await _userManager.FindByIdAsync(Id);
                var response1 = await _userManager.RemovePasswordAsync(user);
                if (response1.Succeeded)
                {
                    var response2 = await _userManager.AddPasswordAsync(user, password);
                    if (response2.Succeeded)
                    {

                        result = "User password has been successfully changed";
                    }
                    else
                    {
                        result = "Failed to add new password (Password either too short," +
                            "Requires Non Alphanumeric Characters and Requires Digits)";
                    }
                }
                else
                {
                    result = "failed to remove the old password";
                }
            }
            catch (Exception ex)
            {
                result = ex.ToString();
            }
            return Json(result);
        }
        public ActionResult GetUsers([FromBody] DataManagerRequest dm)
        {
            var _data = _context.ViewUserManagement.OrderBy(o => o.NameOfUserAccountHolder).ToList();
            IEnumerable data = _data;
            int _count = _data.Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                _count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                _count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = _count });
        }
        public IActionResult UserManagement()
        {
            //ViewBag.Roles = _context.ViewAspNetRoles.Where(o => o.Name != "MobileUser").ToList();

            ViewBag.Roles = _context.ViewAspNetRoles.OrderBy(s => s.Name).ToList();
            ViewBag.department = _context.ADepartment.OrderBy(s => s.Department).ToList();
            ViewBag.organization = _context.AOrganization.OrderBy(s => s.Organization).ToList();

            return View();
        }

        public ActionResult DataSourceUserManagement([FromBody] DataManagerRequest dm)
        {
            //  SummitModels obj = new SummitModels(_context);

            IEnumerable data = _context.ViewUserManagement.Where(a => (a.Deleted == false) && (a.Deleted != null)).OrderBy(a => a.Email).ToList();
            int count = _context.ViewUserManagement.Where(a => (a.Deleted == false) && (a.Deleted != null)).OrderBy(a => a.Email).ToList().Count;

            //IEnumerable data = _context.ViewUserManagementNew.OrderBy(a => a.Email).ToList();
            //int count = _context.ViewUserManagementNew.OrderBy(a => a.Email).ToList().Count;


            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count });

        }
              
        public ActionResult DataSourceUserManagementNew([FromBody] DataManagerRequest dm)
        {

            var _data = _context.ViewUserManagement.OrderBy(a => a.NameOfUserAccountHolder).ToList();
            IEnumerable data = _data;
            int _count = _data.Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                _count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                _count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = _count });

        }
        public async Task<IActionResult> UpdateUserManagement([FromBody] CRUDModel<ViewUserManagement> value)
        {
            var exists = _context.AspNetUsers.Find(value.Value.Id);
            if (exists != null)
            {
               
                exists.LockoutEnabled = value.Value.LockoutEnabled;
                exists.NameOfUserAccountHolder = value.Value.NameOfUserAccountHolder;
                exists.PhoneNumber = value.Value.PhoneNumber;
                exists.Department = value.Value.Department;
                exists.Organization = value.Value.Organization;
                exists.UserName = value.Value.UserName;
                exists.Email = value.Value.Email;
               
                _context.Entry(exists).State = EntityState.Modified;
                _context.SaveChanges();

                var user = await _userManager.FindByIdAsync(value.Value.Id);

                var userRoles = await _userManager.GetRolesAsync(user);
                foreach (string r in userRoles)
                {
                    await _userManager.RemoveFromRoleAsync(user, r);
                }

                await _userManager.AddToRoleAsync(user, value.Value.RoleName);
               
            }
            return Json(value);
        }

        #region ManageFinancialYears
   
        public IActionResult ManageFinancialYears()
        {
            ViewBag.headerTextOne = new TabHeader { Text = "Financial Year", IconCss = "" };
            ViewBag.headerTextTwo = new TabHeader { Text = "Responsibility Center", IconCss = "" };
            ViewBag.headerTextThree = new TabHeader { Text = "Manage MDA", IconCss = "" };
            ViewBag.animation = new string[] { "SlideLeftIn", "SlideRightIn", "FadeIn", "FadeOut", "FadeZoomIn", "FadeZoomOut", "ZoomIn", "ZoomOut", "None" };

            return View();
        }
               
        #endregion ManageFinancialYears


        [HttpGet]
        public ViewResult Register()
        {
            RegisterViewModel m = new RegisterViewModel();
           
            //ViewBag.Roles = _context.ViewAspNetRoles.Where(o => o.Name != "MobileUser").ToList();           

            ViewBag.Roles = _context.ViewAspNetRoles.OrderBy(s => s.Name).ToList();
            ViewBag.department = _context.ADepartment.OrderBy(s => s.Department).ToList();
            ViewBag.organization = _context.AOrganization.OrderBy(s => s.OrganizationId).ToList();
            //ViewBag.org = 1;
            if (ViewBag.organization !=null)
            {
                ViewBag.org = m.organization_id = 3;
                
            }
           
           

            //ViewBag.Directorate = _context.EdpmDirectorate.OrderBy(s => s.Name).ToList();
            //var Sectors = _context.EdpmSectors.OrderBy(s => s.Sector).ToList();
            //ViewBag.Sectors = Sectors;
            //ViewBag.SectoralCouncil = _context.EdpmSectoralCouncil.OrderBy(s => s.Description).ToList();
            //ViewBag.YesNo = _context.AYesNo.OrderBy(s => s.YesNoId).ToList();
            //ViewBag.Institution = _context.EdpmCountries.Where(a => a.Type == "Institution").OrderBy(s => s.Country).ToList();
            //ViewBag.Modules = _context.ASystemModules.ToList();
            //ViewBag.Section = _context.ASystemSection.ToList();
            //ViewBag.Protocol = _context.CommonMarketProtocol.ToList();
            return View(m);
        }

        [HttpPost]
       
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new IdentityUser { UserName = model.UserName, Email = model.Email };
                var result = await _userManager.CreateAsync(user, model.Password);
               
                if (result.Succeeded)
                {
                    var _result = await this._userManager.AddToRoleAsync(user, model.UserRole);
                    if (_result.Succeeded)
                    {
                        //if (model.Modules.Count() > 0)
                        //{
                        //    ASelecetedModules _Modules = new ASelecetedModules()
                        //    {
                        //        UserId = user.Id,
                        //        SelectedModules = string.Join(",", model.Modules)
                        //    };
                        //    _context.ASelecetedModules.Add(_Modules);
                        //    _context.SaveChanges();
                        //}
                       
                        //if (model.Section.Count() > 0)
                        //{
                        //    ASelecetedSections _Section = new ASelecetedSections()
                        //    {
                        //        UserId = user.Id,
                        //        SelectedSectors = string.Join(",", model.Section)
                        //    };
                        //    _context.ASelecetedSections.Add(_Section);
                        //    _context.SaveChanges();
                        //}

                        //if (model.Protocol_id.Count() > 0 || model.sectoralcouncil_id.Count() > 0)
                        //{
                        //    AspNetAssignment assignment = new AspNetAssignment()
                        //    {
                        //        UserId = user.Id,
                        //        CmpAssigned = string.Join(",", model.Protocol_id),
                        //        SectoralCouncilAssigned = string.Join(",", model.sectoralcouncil_id)
                        //    };
                        //    _context.AspNetAssignment.Add(assignment);
                        //    _context.SaveChanges();
                        //}

                        var exists = _context.AspNetUsers.Find(user.Id);
                        if (exists != null)
                        {
                            exists.NameOfUserAccountHolder = model.NameOfUserAccountHolder;
                            exists.Email = model.Email;
                            exists.UserName = model.UserName;
                            exists.PhoneNumber = model.PhoneNumber;
                            exists.Department = model.department_id;
                            exists.Organization = model.organization_id;

                            //exists.PasswordHash = model.Password;
                            //exists.Name = model.UserRole; //User Role

                            //exists.SectoralCouncilId = string.Join(",", model.sectoralcouncil_id);                                                     
                            //exists.Institution = System.Convert.ToInt32(model.institution_id);

                            //exists.Sector = System.Convert.ToInt32(model.sector_id);//Department/Division

                            exists.LockoutEnabled = false;
                            exists.EmailConfirmed = true;

                            exists.Deleted = false;
                            exists.RegisteredBy = userInfor.GetUser().Identity.Name; 
                            exists.RegisteredDate = DateTime.Now;

                            _context.Entry(exists).State = EntityState.Modified;
                            _context.SaveChanges();
                        }
                    }
                    return RedirectToAction("UserManagement", "Account");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            //ViewBag.Roles = _context.ViewAspNetRoles.Where(o => o.Name != "MobileUser").ToList();
            
            ViewBag.Roles = _context.ViewAspNetRoles.OrderBy(s => s.Name).ToList();
            
            //ViewBag.Countries = _context.EdpmCountries.Where(a => a.Id == 1 || a.Id == 2 || a.Id == 3 || a.Id == 4 || a.Id == 5 || a.Id == 9 || a.Id == 42).OrderBy(s => s.Country).ToList();
            //ViewBag.Directorate = _context.EdpmDirectorate.OrderBy(s => s.Name).ToList();
            //var Sectors = _context.EdpmSectors.OrderBy(s => s.Sector).ToList();
            //ViewBag.Sectors = Sectors;
            //ViewBag.SectoralCouncil = _context.EdpmSectoralCouncil.OrderBy(s => s.Description).ToList();
            //ViewBag.YesNo = _context.AYesNo.OrderBy(s => s.Id).ToList();
            //ViewBag.Institution = _context.EdpmCountries.Where(a => a.Type == "Institution").OrderBy(s => s.Country).ToList();
            //ViewBag.Modules = _context.ASystemModules.ToList();
            //ViewBag.Section = _context.ASystemSection.ToList();
            //ViewBag.Protocol = _context.CommonMarketProtocol.ToList();

            return View();
        }

        //[HttpPost]
        //public async Task<IActionResult> Logout()
        //{
        //    await _signInManager.SignOutAsync();
        //    return RedirectToAction("Login", "Account");
           
        //}

        public async Task<IActionResult>Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = "")
        {
            var model = new LoginViewModel { ReturnUrl = returnUrl };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                //var user = _context.AspNetUsers1.FirstOrDefault(o => (o.Email == model.Username || o.UserName == model.Username));
                var user = _context.AspNetUsers.FirstOrDefault(o => (o.Email == model.Username || o.UserName == model.Username));

                // var user = _context.AspNetUsers1.FirstOrDefault(o => o.UserName == model.Username);

                if (user != null)
                {
                    model.Username = user.UserName;
                }

                var result = await _signInManager.PasswordSignInAsync(model.Username,
                   model.Password, model.RememberMe, false);


                if (result.Succeeded)
                {
                    //var roles = _context.ViewUsersRoles.FirstOrDefault(a => a.UserName == model.Username);
                    var roles = _context.ViewUsersRoles.FirstOrDefault(a => (a.UserName == model.Username) && (a.Deleted == false));
                                        
                    if (roles != null)
                    {
                        //SaveLog(model);
                        //return RedirectToAction("DashBoard", "Home");
                        return RedirectToAction("HomePage", "Home");
                    }

                    else if (roles == null)
                    {                       
                        return RedirectToAction("LoginUnAuth", "Account");

                    }
                }
            }
            
            ModelState.AddModelError("", "Invalid login attempt");
            return View(model);
        }

        private void SaveLog(LoginViewModel model)
        {
            try
            {
                var user = _context.AspNetUsers.FirstOrDefault(o => (o.Email == model.Username || o.UserName == model.Username));
                string location = RetrieveFormatedAddress(model.latitude, model.Longitude);
                int count = _context.ALoginLog.ToList().Count;
                var login = new ALoginLog();
                login.Id = (count + 1);
                login.UserId = user.Id;
                login.UserName = model.Username;
                login.LoginDate = DateTime.Now;
                login.Latitude = model.latitude;
                login.Longitude = model.Longitude;
                login.LocDescription = location;
                _context.ALoginLog.Add(login);
                _context.SaveChanges();
            }
            catch
            {

            }

        }
        static string baseUri = "https://maps.googleapis.com/maps/api/place/nearbysearch/xml?location={0},{1}&radius=150&type=hospital&fields=formatted_address,name&key=AIzaSyDUL9cVq09KKrWa4PmlfFZ2DTVZz-DgI0Q";
        public string RetrieveFormatedAddress(double lat, double lng)
        {
            try
            {
                string location = string.Empty;
                if (!double.IsNaN(lat) && !double.IsNaN(lng))
                {
                    string requestUri = string.Format(baseUri, lat, lng);

                    using (WebClient wc = new WebClient())
                    {
                        string result = wc.DownloadString(requestUri);
                        var xmlElm = XElement.Parse(result);
                        var status = (from elm in xmlElm.Descendants()
                                      where
                        elm.Name == "status"
                                      select elm).FirstOrDefault();
                        if (status.Value.ToLower() == "ok")
                        {
                            var res = (from elm in xmlElm.Descendants()
                                       where
                            elm.Name == "name"
                                       select elm).FirstOrDefault();
                            location = res.Value;

                            var _res = (from elm in xmlElm.Descendants()
                                        where
                             elm.Name == "vicinity"
                                        select elm).FirstOrDefault();
                            location += ", " + _res.Value;

                        }
                    }
                }

                return location;
            }
            catch
            {
                return string.Empty;
            }
        }

        public ActionResult UpdateUserInformation([FromBody] CRUDModel<ViewUserManagement> value)
        {
            // ((IObjectContextAdapter)this._context).ObjectContext.CommandTimeout = 3000;
            //Performing insert operation
            if (value.Added != null && value.Added.Count() > 0)
            {
                //foreach (var temp in added)
                {

                }
            }

            if (value.Changed != null && value.Changed.Count() > 0)
            {
                foreach (var temp in value.Changed)
                {
                    // temp.EditedBy = User.Identity.GetUserId();
                    // temp.EditedDate = DateTime.Now;
                    var check = _context.AspNetUsers.FirstOrDefault(o => o.Id == temp.Id);

                    //var check = _context.ViewUserManagementNew.FirstOrDefault(o => o.Id == temp.Id);

                    check.UserName = temp.UserName;                   
                    check.Email = temp.Email;
                    check.PhoneNumber = temp.PhoneNumber;

                    //check.RoleName = temp.RoleName;

                    check.LockoutEnd = temp.LockoutEnd;
                    check.LockoutEnabled = temp.LockoutEnabled;


                    _context.Entry(check).State = EntityState.Modified;
                    _context.SaveChanges();
                   

                }
            }
            //Performing delete operation
            if (value.Deleted != null && value.Deleted.Count() > 0)
            {
                foreach (var temp in value.Deleted)
                {
                    _context.AspNetUsers.Remove(_context.AspNetUsers.FirstOrDefault(o => o.Id == temp.Id));
                    _context.SaveChanges();
                }
            }

            //dbCase.SaveChanges();
            //obj.Update();
            return Content("") /*RedirectToAction("DataSourceCommonMarket")*/;
        }

        public ActionResult DeleteRecord(AspNetUsers value, string id)
        {

            AspNetUsers table = _context.AspNetUsers.FirstOrDefault(o => o.Id == id);

            if (table != null)
            {
                table.Deleted = true;
                table.DeletedBy = userInfor.GetUser().Identity.Name;
                table.DeletedDate = DateTime.Now;

                _context.Entry(table).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                   
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }

            return Json(null);
            //return Json(value, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RetrieveRecord(AspNetUsers value, string id)
        {
            AspNetUsers table = _context.AspNetUsers.FirstOrDefault(o => o.Id == id);

            if (table != null)
            {
                table.Deleted = false;
                table.RetrievedBy = userInfor.GetUser().Identity.Name;
                table.RetrievedDate = DateTime.Now;

                _context.Entry(table).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                    // TempData["Success"] = "Record Saved Successfully!";
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }

            return Json(null);
            //return Json(value, JsonRequestBehavior.AllowGet);
        }

        #region DeletedUsers

        public ActionResult DataSourceDeletedUsers([FromBody] DataManagerRequest dm)
        {
            //  SummitModels obj = new SummitModels(_context);

            IEnumerable data = _context.ViewUserManagement.Where(a => a.Deleted == true).OrderBy(a => a.Email).ToList();
            int count = _context.ViewUserManagement.Where(a => a.Deleted == true).OrderBy(a => a.Email).ToList().Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count });

        }
             
        public ActionResult DeletedUsers()
        {
            var _logins = _context.ALoginLog.AsNoTracking().ToList();
            var logins = _logins.Where(o => Convert.ToDateTime(o.LoginDate).Date == DateTime.Now.Date).ToList().Count;
            ViewBag.LogCount = logins;
           
            //ViewBag.Country = _context.EdpmCountries.AsNoTracking().Where(a => a.Id != 1 && a.Id != 48).OrderBy(b => b.Type).ToList();
                        
            //ViewBag.Institution = _context.EdpmCountries.AsNoTracking().Where(c => c.Type == "Institution").OrderBy(d => d.Country).ToList();
                        
            //ViewBag.SectoralCouncil = _context.EdpmSectoralCouncil.AsNoTracking().ToList();
                        
            ViewBag.YESNO = _context.AYesNo.AsNoTracking().ToList();
                        
            //ViewBag.Directorate = _context.EdpmDirectorate.AsNoTracking().OrderBy(a => a.Name).ToList();

            //ViewBag.Sector = _context.EdpmSectors.AsNoTracking().OrderBy(a => a.Sector).ToList();
            
            //ViewBag.Roles = _context.ViewAspNetRoles.OrderBy(s => s.Name);


            return View();
        }

        public ActionResult LoginUnAuth()
        {           
            return View();
        }

        #endregion


        #region ManageMDA
        public IActionResult ManageMDA()
        {
            ViewBag.headerTextOne = new TabHeader { Text = "Financial Year", IconCss = "" };
            ViewBag.headerTextTwo = new TabHeader { Text = "Responsibility Center", IconCss = "" };
            ViewBag.headerTextThree = new TabHeader { Text = "Manage MDA", IconCss = "" };
            ViewBag.animation = new string[] { "SlideLeftIn", "SlideRightIn", "FadeIn", "FadeOut", "FadeZoomIn", "FadeZoomOut", "ZoomIn", "ZoomOut", "None" };

            return View();
        }

        //public ActionResult DataSourceMDA([FromBody] DataManagerRequest dm)
        //{
        //    var _data = _context.CommonMarketMda.AsNoTracking().OrderByDescending(a => a.MdaId).Where(a => (a.Deleted == false) || (a.Deleted == null)).ToList();
        //    IEnumerable data = _data;
        //    int _count = _data.Count;
            
        //    DataOperations operation = new DataOperations();
        //    //Performing filtering operation
        //    if (dm.Where != null)
        //    {
        //        data = operation.PerformFiltering(data, dm.Where, "and");
        //        var filtered = (IEnumerable<object>)data;
        //        _count = filtered.Count();
        //    }

        //    //Performing search operation
        //    if (dm.Search != null)
        //    {
        //        data = operation.PerformSearching(data, dm.Search);
        //        var searched = (IEnumerable<object>)data;
        //        _count = searched.Count();
        //    }

        //    //Performing sorting operation
        //    if (dm.Sorted != null)
        //        data = operation.PerformSorting(data, dm.Sorted);

        //    //Performing paging operations
        //    if (dm.Skip > 0)
        //        data = operation.PerformSkip(data, dm.Skip);
        //    if (dm.Take > 0)
        //        data = operation.PerformTake(data, dm.Take);

        //    return Json(new { result = data, count = _count });
        //}

        //public ActionResult DialogueSaveMDA([FromBody] CRUDModel<CommonMarketMda> value)
        //{
        //    string mdadet = null;
        //    string mdadesc = null;

        //    if (!string.IsNullOrEmpty(value.Value.MdaDetails))
        //    {
        //        mdadet = (value.Value.MdaDetails).Trim();
        //    }

        //    if (!string.IsNullOrEmpty(value.Value.Mdadesc))
        //    {
        //        mdadesc = (value.Value.Mdadesc).Trim();
        //    }

        //    CommonMarketMda _table = _context.CommonMarketMda.FirstOrDefault(o =>(o.MdaId == value.Value.MdaId));
                       
        //    if (_table == null)
        //    {
        //        //Save into Core_CaseContacts table

        //        CommonMarketMda table = new CommonMarketMda();
               

        //        table.MdaId = value.Value.MdaId;
        //        table.MdaDetails = mdadet;
        //        table.Mdadesc = mdadesc;
        //        table.Deleted = false;

        //        //table.MdaDetails = (value.Value.MdaDetails).Trim();
        //        //table.Mdadesc = (value.Value.Mdadesc).Trim();

        //        _context.CommonMarketMda.Add(table);
        //        _context.SaveChanges();

        //    }
        //    else
        //    {
        //        this.DialogUpdate(value);
        //    }
        //    return Json(value.Value);
        //}

        //public ActionResult DialogUpdate([FromBody] CRUDModel<CommonMarketMda> value)
        //{
        //    CommonMarketMda table = _context.CommonMarketMda.FirstOrDefault(o => (o.MdaId == value.Value.MdaId));

        //    string mdadet = null;
        //    string mdadesc = null;

        //    if (!string.IsNullOrEmpty(value.Value.MdaDetails))
        //    {
        //        mdadet = (value.Value.MdaDetails).Trim();
        //    }

        //    if (!string.IsNullOrEmpty(value.Value.Mdadesc))
        //    {
        //        mdadesc = (value.Value.Mdadesc).Trim();
        //    }

        //    if (table != null)
        //    {
        //        //table.MdaDetails = value.Value.MdaDetails;
        //        //table.Mdadesc = value.Value.Mdadesc;

        //        table.MdaDetails = mdadet;
        //        table.Mdadesc = mdadesc;
        //        table.Deleted = false;

        //        _context.Entry(table).State = EntityState.Modified;
        //        _context.SaveChanges();

        //        try
        //        {

        //            TempData["Success"] = "Record Saved Successfully!";
        //        }


        //        catch (DbEntityValidationException dbEx)
        //        {
        //            foreach (var validationErrors in dbEx.EntityValidationErrors)
        //            {
        //                foreach (var validationError in validationErrors.ValidationErrors)
        //                {
        //                    TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
        //                    System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
        //                }
        //            }
        //        }
        //    }
           
        //    return Json(value.Value);
        //}

        //public ActionResult CheckMDACode()
        //{
        //    int result = 0;
        //    var data = _context.CommonMarketMda.OrderByDescending(o => o.MdaId).FirstOrDefault();
        //    if (data != null)
        //    {
        //        result = data.MdaId;
        //    }
        //    return Json(result);

        //}

        #endregion ManageMDA
    }
}