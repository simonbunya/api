﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using API_System.BLL;
using API_System.Models;

namespace API_System.Controllers
{
    public class HomeController : Controller
    {
      // private readonly SignInManager<IdentityUser> _signInManager;
        private API_SystemContext _context;
        private IServiceApi _service;
        //   private readonly UserManager<IdentityUser> _userManager;

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, API_SystemContext context, IServiceApi service)
        {
            _logger = logger;
            this._context = context;
            _service = service;
        }
               
        public async Task<IActionResult> Index()
        {

            ///_service.SaveChilliData();

            //API_Service m = new API_Service();

            //var ChilliData = await m.GetRecords();
            //var CrcData = await m.GetCRCRecords();
            //var LgroupsData = await m.GetLgroupsRecords();
            //var McareGroupsData = await m.GetMCareGroupsRecords();
            //var SamplegrpsData = await m.GetsamplegrpsRecords();


            //var McareGroupsData = m.GetMCareGroupsRecordss();

            //await UploadData(ChilliData);
            return View();
        }

        // [HttpPost("UploadData")]
        //[ProducesResponseType(200)]
        //[ProducesResponseType(500)]
        //public async Task<ActionResult> UploadData(UploadedData uploads)
        //{
        //    try
        //    {
        //        //string code = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(uploads.AccessCode));
        //        //var loginvalid = await _context.MobileLogin.FirstOrDefaultAsync(o => o.LoginCode == uploads.AccessCode && o.ExpiryDate > DateTime.Now);
        //        //if (loginvalid == null) { return NotFound(); }

        //        if (uploads.Ona1Chilligrps != null)
        //        {
        //            foreach (var n in uploads.Ona1Chilligrps)
        //            {
        //                var exists = _context.Ona1Chilligrps.FirstOrDefault(o => o.CaseNo == n.CaseNo);
        //                if (exists == null)
        //                {
        //                    _context.Ona1Chilligrps.Add(n);
        //                }
        //                else
        //                {
        //                    _context.Entry(exists).CurrentValues.SetValues(n);
        //                    _context.Entry(exists).State = EntityState.Modified;
        //                }
        //                await _context.SaveChangesAsync();

        //            }
        //        }

        //        return null;
              
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex);
        //    }
        //}
        public IActionResult DashBoard()
        {
            ViewBag.ChilligrpsPath = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\Chilligrps.sydx";
            ViewBag.CRC = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\CRC.sydx";
            ViewBag.Lgroups = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\Lgroups.sydx";
            ViewBag.MCareGroups = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\MCareGroups.sydx";
            ViewBag.CGAssessment = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\CGAssessmentDashboard.sydx";
            ViewBag.AGYW2020 = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\OnaAGYW2020.sydx";
            ViewBag.HHProfile = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\HHProfile.sydx";
            ViewBag.BSPSurvRev = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\BSP_Survey_finalNew.sydx";
            ViewBag.Livelihood = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\GeneralPopulationDashboard.sydx";
                  
            return View();
        }

        public IActionResult LivelihoodVSLA()
        {          
            // ViewBag.LivelihoodVSLA = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\LivelihoodCountsDashboard.sydx";
            ViewBag.LivelihoodVSLA = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\VSLADashBoard.sydx";
            //ViewBag.LivelihoodVSLA = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\LinkagesMarketsDashboard.sydx";
            return View();
        }

        public IActionResult LivelihoodLinkageMkts()
        {            
            ViewBag.LivelihoodLinkageMkts = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\LinkagesMarketsDashboard.sydx";
            return View();
        }

        public IActionResult MICYANGroups()
        {
            ViewBag.MICYANGroups = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\MICYANGroups.sydx";
            return View();
        }

        public IActionResult NutritionTraining()
        {
            ViewBag.NutritionTraining = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\NutritionTraining.sydx";
            return View();
        }

        public IActionResult IYCF()
        {
            ViewBag.IYCF = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\IYCFRelatedInterventions.sydx";
            return View();
        }

        public IActionResult SchoolInterventions()
        {
            ViewBag.SchoolInterventions = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\SchoolInterventions.sydx";
            return View();
        }

        public IActionResult AGYW()
        {
            ViewBag.AGYW = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\AGYWNew.sydx";
            return View();
        }

        public IActionResult EnvManagement()
        {
            ViewBag.EnvMan = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\EnvCommGroupSummary.sydx";
            return View();
        }

        public IActionResult BSPRevised()
        {
            ViewBag.BSPRevised = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\BSPSurveyRevised.sydx";
            return View();
        }

        
        public IActionResult HomePage()
        {
            
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
