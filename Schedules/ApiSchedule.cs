﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_System.Models;

namespace API_System.Schedules
{
    public class ApiSchedule : IJob
    {
        private IServiceApi _service;
        //public ApiSchedule(IServiceApi service)
        //{
        //    _service = service;
        //}
        public Task Execute(IJobExecutionContext context)
        {
            LoadData();
            return Task.CompletedTask;
        }

        //public static async void LoadData()
        public async void LoadData()
        {
            using (var serviceScope = ServiceActivator.GetScope())
            {
                _service = (IServiceApi)serviceScope.ServiceProvider.GetService(typeof(IServiceApi));
            }
        }
    }
}
