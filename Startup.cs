using System;
using Syncfusion.Licensing;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using API_System.Models;
using API_System.BLL;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using API_System.Data;
using Microsoft.AspNetCore.Http;
using Quartz.Spi;
using API_System.Schedules;
using Quartz;
using Quartz.Impl;
using Microsoft.EntityFrameworkCore.SqlServer.Internal;

namespace API_System
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            SyncfusionLicenseProvider.RegisterLicense("MDAxQDMxMzgyZTMyMmUzMEhRMEtUZ2ZZSTNhRXRLNHp5Z3h1MkFEUTAwbENjVG9jdHFpUXBaQVJ5M0U9");
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(
            Configuration.GetConnectionString("DefaultConnection")));

            //Remove Transient Failure Problem 
            services.AddDbContext<API_SystemContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
            sqlServerOptionsAction: sqlOptions =>
            {
                sqlOptions.EnableRetryOnFailure();
            }

            ));
            //Remove Transient Failure Problem 

            services.AddDbContext<API_SystemContext>(options =>
               options.UseSqlServer(
                   Configuration.GetConnectionString("DefaultConnection"), sqloptions => sqloptions.CommandTimeout(3000)).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));
        
            //SqlServerOptionsActions: sqlOptions =>
            //{
            //    sqlOptions.EnableRetryonFailure();
            //});
            
            //services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
            //    .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IServiceApi, API_Service>();
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });
            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
               .AddRoles<IdentityRole>()
               .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddControllersWithViews();
            services.AddHttpContextAccessor();
            services.AddTransient<IUserService, UserService>();
            services.AddControllersWithViews();
            services.AddRazorPages();
            services.AddMvc();
            // Add Quartz services
            services.AddSingleton<IJobFactory, SingletonJobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();

            // Add our job
            //services.AddScoped<IJob,ApiSchedule>();
            //services.AddTransient<IJob,ApiSchedule>();
            services.AddSingleton<ApiSchedule>();
            services.AddSingleton(new JobSchedule(
                jobType: typeof(ApiSchedule),
                cronExpression: "0 0/10 0-23 ? * MON-SUN"));
            /*cronExpression: "0 0/30 7-17 ? * MON-SAT"));*/ // run every 1 hour from monday to sato fron 7:30 to 5:30 pm
            services.AddHostedService<QuartzHostedService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("Mjg1MzQyQDMxMzgyZTMyMmUzMGFkRXJFK2VvTnZJTVorQU8yTS9XZ1BaY0RaamZRc2VCa3VFVjRBZWZzT2s9");
            ServiceActivator.Configure(app.ApplicationServices);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
               pattern: "{controller=Account}/{action=Login}/{id?}");
                //pattern: "{controller=Home}/{action=DashBoard}/{id?}");
                endpoints.MapRazorPages();
            });
        }


    }
}


//New Import

/*Scaffold-DbContext "Server=ttemakasse;Database=API_System;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models*/

//Update With Context Name

//Scaffold-DbContext "Server=ttemakasse;Database=API_System;User Id=sa;password=8686;Trusted_Connection=False;MultipleActiveResultSets=true;" Microsoft.EntityFrameworkCore.SqlServer -o Models -f -context "API_SystemContext"

//Update

//Scaffold-DbContext -Connection "Data Source=ttemakasse;Database=API_System;Integrated Security=True;Trusted_Connection=True;" -Provider Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models -force
