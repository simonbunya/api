﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_System.ModelView
{
    public class RegisterViewModel
    {

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        //[EmailAddress]
        [Display(Name = "Name")]
        public string NameOfUserAccountHolder { get; set; }

        [Required]
        [Display(Name = "UserName")]
        public string UserName { get; set; }
        //[Required]
        //[EmailAddress]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "User Role")]
        public string UserRole { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Department")]
        public int? department_id { get; set; }

        [Display(Name = "Organization")]
        public int? organization_id { get; set; }


        [Display(Name = "Select Modules(s)")]
        public int[] Modules { get; set; }

        [Display(Name = "Select Section(s)")]
        public int[] Section { get; set; }

        [Display(Name = "Facility / School")]
        //public int? FacilityCode { get; set; }
        public int[] Facility { get; set; }

        
    }
}
