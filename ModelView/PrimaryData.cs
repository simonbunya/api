﻿using USAID_ICAN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace USAID_ICAN.ModelView
{
    public class PrimaryData
    {
        public List<AClassification> AClassification { get; set; }
        public List<ACountry> ACountry { get; set; }
        public List<ACounty> ACounty { get; set; }
        public List<ADays> ADays { get; set; }
        public List<ADistrict> ADistrict { get; set; }
        public List<AFacilityUnit> AFacilityUnit { get; set; }
        public List<AExposureType> AExposureType { get; set; }
        public List<ALabResults> ALabResults { get; set; }
        public List<AOccupation> AOccupation { get; set; }
        public List<AOperationalArea> AOperationalArea { get; set; }
        public List<AOutcome> AOutcome { get; set; }
        public List<AParish> AParish { get; set; }
        public List<ARelationship> ARelationship { get; set; }
        public List<ARiskLevel> ARiskLevel { get; set; }
        public List<ASex> ASex { get; set; }
        public List<ASubcounty> ASubcounty { get; set; }
        public List<ASymptoms> ASymptoms { get; set; }
        public List<ATitle> ATitle { get; set; }
        public List<AVillage> AVillage { get; set; }
        public List<AYesNo> AYesNo { get; set; }
        public List<ViewEmployee> Employee { get; set; }
        public List<ADiseaseType> ADiseaseType { get; set; }
        public List<VwAdminUnits> VwAdminUnits { get; set; }
        public List<ViewSitRepPrimary> ViewSitRepPrimary { get; set; }
        public List<CoreCases> ViewCases { get; set; }
        public List<CoreCaseContacts> ViewContacts { get; set; }
        public List<CoreComplianceVisit> ViewComplianceVisit { get; set; }
        public List<CoreCommunitySurveillance> ViewSurveillance { get; set; }
        public List<CorePublicTransport> corePublicTransport { get; set; }
        public List<CorePublicTransportDetials> corePublicTransportDetials { get; set; }
        public List<ViewFollowUp> FollowUp { get; set; }
        public List<ViewFollowupTests> FollowUpTest { get; set; }

    }
    public class SucessLoginCredentials
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string UserRole { get; set; }
        public int EmployeeId { get; set; }
        public string AccessToken { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string assignedfacilities { get; set; }
        public string assignedModules { get; set; }
    }
    public class LoginCredentials
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class UploadedData
    {
        public string AccessCode { get; set; }
        public List<CoreCaseContacts> CoreCaseContacts { get; set; }
        public List<CoreCaseContactsMonitoring> CoreCaseContactsMonitoring { get; set; }
        public List<CoreCases> CoreCases { get; set; }
        public List<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
        public List<CoreComplianceVisit> CoreComplianceVisit { get; set; }
        public List<CorePublicTransport> CorePublicTransport { get; set; }
        public List<CorePublicTransportDetials> CorePublicTransportDetials { get; set; }
        public List<CoreSitRep> CoreSitRep { get; set; }
        public List<CoreSitRepHeader> CoreSitRepHeader { get; set; }
    }
    public class UploadReturnData
    {
        public List<AClassification> AClassification { get; set; }
        public List<ACountry> ACountry { get; set; }
        public List<ACounty> ACounty { get; set; }
        public List<ADays> ADays { get; set; }
        public List<ADistrict> ADistrict { get; set; }
        public List<AFacilityUnit> AFacilityUnit { get; set; }
        public List<AExposureType> AExposureType { get; set; }
        public List<ALabResults> ALabResults { get; set; }
        public List<AOccupation> AOccupation { get; set; }
        public List<AOperationalArea> AOperationalArea { get; set; }
        public List<AOutcome> AOutcome { get; set; }
        public List<AParish> AParish { get; set; }
        public List<ARelationship> ARelationship { get; set; }
        public List<ARiskLevel> ARiskLevel { get; set; }
        public List<ASex> ASex { get; set; }
        public List<ASubcounty> ASubcounty { get; set; }
        public List<ASymptoms> ASymptoms { get; set; }
        public List<ATitle> ATitle { get; set; }
        public List<AVillage> AVillage { get; set; }
        public List<AYesNo> AYesNo { get; set; }
        public List<ViewEmployee> Employee { get; set; }
        public List<ADiseaseType> ADiseaseType { get; set; }
        public List<VwAdminUnits> VwAdminUnits { get; set; }
        public List<ViewSitRepPrimary> ViewSitRepPrimary { get; set; }
        public List<CoreCases> ViewCases { get; set; }
        public List<CoreCaseContacts> ViewContacts { get; set; }
    }
    public class ChangePassword
    {
        public string AccessCode { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}
