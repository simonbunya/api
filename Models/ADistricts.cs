﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ADistricts
    {
        public int Id { get; set; }
        public int RegionId { get; set; }
        public string LocDistrict { get; set; }
    }
}
