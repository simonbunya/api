﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewCommgroupsummarytrainedbmgtskillstraining
    {
        public double Id { get; set; }
        public string Start { get; set; }
        public string Endd { get; set; }
        public string District { get; set; }
        public string Subcounty { get; set; }
        public string Village { get; set; }
        public string Groupname { get; set; }
        public string Acty { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string Tech { get; set; }
        public string Mgt { get; set; }
    }
}
