﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewMiycanmemberstrainedinChildSpacing
    {
        public double Id { get; set; }
        public string District { get; set; }
        public string Subcounty { get; set; }
        public string Village { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public int? Session41 { get; set; }
        public int? Session42 { get; set; }
        public int? Session43a { get; set; }
        public int? Session43b { get; set; }
        public int? Session44 { get; set; }
        public int? Session45ChildSpacing { get; set; }
    }
}
