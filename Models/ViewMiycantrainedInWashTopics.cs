﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewMiycantrainedInWashTopics
    {
        public double Id { get; set; }
        public string District { get; set; }
        public string Subcounty { get; set; }
        public string Village { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public int? Session11 { get; set; }
        public int? Session12 { get; set; }
        public int? Session13 { get; set; }
        public int? Session14aWash { get; set; }
        public int? Session14bWash { get; set; }
        public int? Session14c { get; set; }
        public int? Session15 { get; set; }
        public int? Session16a { get; set; }
        public int? Session16bWash { get; set; }
        public int? Session16c { get; set; }
        public int? Session17 { get; set; }
    }
}
