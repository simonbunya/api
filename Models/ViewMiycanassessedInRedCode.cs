﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewMiycanassessedInRedCode
    {
        public double Id { get; set; }
        public string District { get; set; }
        public string Groupid { get; set; }
        public string Subcounty { get; set; }
        public string Village { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public double? RedMale5 { get; set; }
        public double? RedFemale5 { get; set; }
        public double? RedMale2 { get; set; }
        public double? RedFemale2 { get; set; }
        public string Region { get; set; }
    }
}
