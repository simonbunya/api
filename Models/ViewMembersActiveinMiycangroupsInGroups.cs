﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewMembersActiveinMiycangroupsInGroups
    {
        public double Id { get; set; }
        public string Mod { get; set; }
        public int ModeCount { get; set; }
        public DateTime? SubmissionTime { get; set; }
    }
}
