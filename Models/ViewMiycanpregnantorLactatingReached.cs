﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewMiycanpregnantorLactatingReached
    {
        public double Id { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public double? L19 { get; set; }
        public double? P19 { get; set; }
        public double? PA19 { get; set; }
        public double? LA19 { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public string Subcounty { get; set; }
        public string Village { get; set; }
    }
}
