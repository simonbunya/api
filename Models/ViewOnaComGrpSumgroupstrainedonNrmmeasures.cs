﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewOnaComGrpSumgroupstrainedonNrmmeasures
    {
        public string LocationGroupid { get; set; }
        public string District { get; set; }
        public string Subcounty { get; set; }
        public string Village { get; set; }
        public string Monthh { get; set; }
        public string Year { get; set; }
        public string Acty { get; set; }
        public string Tr { get; set; }
        public string Region { get; set; }
        public double Id { get; set; }
    }
}
