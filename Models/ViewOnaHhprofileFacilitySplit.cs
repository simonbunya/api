﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewOnaHhprofileFacilitySplit
    {
        public double Id { get; set; }
        public int? Fac { get; set; }
        public string Description { get; set; }
    }
}
