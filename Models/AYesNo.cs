﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AYesNo
    {
        public int YesNoId { get; set; }
        public string YesNoDesc { get; set; }
    }
}
