﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AParishes
    {
        public int Id { get; set; }
        public int SubCountyId { get; set; }
        public string LocParish { get; set; }
    }
}
