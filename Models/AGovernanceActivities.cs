﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AGovernanceActivities
    {
        public AGovernanceActivities()
        {
            CoreCommunityGroupSummary = new HashSet<CoreCommunityGroupSummary>();
        }

        public int GovernId { get; set; }
        public string GovernName { get; set; }

        public virtual ICollection<CoreCommunityGroupSummary> CoreCommunityGroupSummary { get; set; }
    }
}
