﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AInstitutionMappingtype
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
