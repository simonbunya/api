﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ADepartment
    {
        public int DepartmentId { get; set; }
        public string Department { get; set; }
    }
}
