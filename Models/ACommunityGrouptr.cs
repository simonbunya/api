﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ACommunityGrouptr
    {
        public ACommunityGrouptr()
        {
            CoreCommunityGroupSummary = new HashSet<CoreCommunityGroupSummary>();
        }

        public int TrainingId { get; set; }
        public string TrainingName { get; set; }

        public virtual ICollection<CoreCommunityGroupSummary> CoreCommunityGroupSummary { get; set; }
    }
}
