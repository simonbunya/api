﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ASubCounties
    {
        public int Id { get; set; }
        public int DistrictId { get; set; }
        public string LocSubcounty { get; set; }
    }
}
