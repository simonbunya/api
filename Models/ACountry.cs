﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ACountry
    {
        public string CountryCode { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
    }
}
