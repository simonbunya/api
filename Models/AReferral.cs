﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AReferral
    {
        public AReferral()
        {
            CoreMiycanmonthlySummary = new HashSet<CoreMiycanmonthlySummary>();
        }

        public int ReferralId { get; set; }
        public string ReferralName { get; set; }

        public virtual ICollection<CoreMiycanmonthlySummary> CoreMiycanmonthlySummary { get; set; }
    }
}
