﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ADistrict
    {
        public ADistrict()
        {
            ACounty = new HashSet<ACounty>();
            AFieldOfficer = new HashSet<AFieldOfficer>();
            ASubcounty = new HashSet<ASubcounty>();
            CoreCommunityGroupSummary = new HashSet<CoreCommunityGroupSummary>();
            CoreCrcweeklySummary = new HashSet<CoreCrcweeklySummary>();
            CoreMiycanmonthlySummary = new HashSet<CoreMiycanmonthlySummary>();
        }

        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? RegionId { get; set; }
        public bool? IsUrban { get; set; }
        public int? DistrictMinistryCode { get; set; }
        public bool? IsMunicipality { get; set; }

        public virtual ICollection<ACounty> ACounty { get; set; }
        public virtual ICollection<AFieldOfficer> AFieldOfficer { get; set; }
        public virtual ICollection<ASubcounty> ASubcounty { get; set; }
        public virtual ICollection<CoreCommunityGroupSummary> CoreCommunityGroupSummary { get; set; }
        public virtual ICollection<CoreCrcweeklySummary> CoreCrcweeklySummary { get; set; }
        public virtual ICollection<CoreMiycanmonthlySummary> CoreMiycanmonthlySummary { get; set; }
    }
}
