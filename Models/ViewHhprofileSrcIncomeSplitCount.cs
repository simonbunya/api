﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewHhprofileSrcIncomeSplitCount
    {
        public double Id { get; set; }
        public int? SrcIncome { get; set; }
        public string Description { get; set; }
    }
}
