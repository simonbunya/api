﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ANutritionRelatedActivities
    {
        public ANutritionRelatedActivities()
        {
            CoreCommunityGroupSummary = new HashSet<CoreCommunityGroupSummary>();
        }

        public int NutritionId { get; set; }
        public string NutritionName { get; set; }

        public virtual ICollection<CoreCommunityGroupSummary> CoreCommunityGroupSummary { get; set; }
    }
}
