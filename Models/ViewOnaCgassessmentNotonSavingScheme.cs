﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewOnaCgassessmentNotonSavingScheme
    {
        public int? NotonGroupSavingScheme { get; set; }
    }
}
