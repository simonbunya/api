﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewAspNetRoles
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
