﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewMcaremembersinMiycangroups
    {
        public double Id { get; set; }
        public string McareGroupId { get; set; }
        public string MiycanGroupId { get; set; }
        public string Groupname { get; set; }
        public double Expr2 { get; set; }
    }
}
