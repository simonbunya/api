﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewUserManagement
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public string RoleId1 { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string NameOfUserAccountHolder { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public bool? Deleted { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string RetrievedBy { get; set; }
        public DateTime? RetrievedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string RegisteredBy { get; set; }
        public DateTime? RegisteredDate { get; set; }
        public DateTimeOffset? LockoutEnd { get; set; }
        public int? Department { get; set; }
        public int? Organization { get; set; }
    }
}
