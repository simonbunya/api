﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ASubcounty
    {
        public ASubcounty()
        {
            AParish = new HashSet<AParish>();
            CoreCommunityGroupSummary = new HashSet<CoreCommunityGroupSummary>();
            CoreCrcweeklySummary = new HashSet<CoreCrcweeklySummary>();
            CoreMiycanmonthlySummary = new HashSet<CoreMiycanmonthlySummary>();
        }

        public int SubcountyId { get; set; }
        public int? DistrictId { get; set; }
        public int? CountyId { get; set; }
        public string SubcountyName { get; set; }
        public int? SubcountyMinistryCode { get; set; }

        public virtual ADistrict District { get; set; }
        public virtual ICollection<AParish> AParish { get; set; }
        public virtual ICollection<CoreCommunityGroupSummary> CoreCommunityGroupSummary { get; set; }
        public virtual ICollection<CoreCrcweeklySummary> CoreCrcweeklySummary { get; set; }
        public virtual ICollection<CoreMiycanmonthlySummary> CoreMiycanmonthlySummary { get; set; }
    }
}
