﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AVillages
    {
        public int Id { get; set; }
        public int ParishId { get; set; }
        public string Loc { get; set; }
    }
}
