﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AInstitutionMappingfi
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
