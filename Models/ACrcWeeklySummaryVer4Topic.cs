﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ACrcWeeklySummaryVer4Topic
    {
        public int Id { get; set; }
        public string TopicDescription { get; set; }
    }
}
