﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AMaritalStatus
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
