﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewCommGpSumIndGpBsdSvgs
    {
        public double Id { get; set; }
        public string District { get; set; }
        public string Subcounty { get; set; }
        public string Village { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public double? MaleY { get; set; }
        public double? MaleA { get; set; }
        public double? FemaleY { get; set; }
        public double? FemaleA { get; set; }
        public string Region { get; set; }
    }
}
