﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AParish
    {
        public AParish()
        {
            AVillage = new HashSet<AVillage>();
        }

        public int ParishId { get; set; }
        public string ParishName { get; set; }
        public int? SubcountyId { get; set; }
        public int? ParishMinistryCode { get; set; }

        public virtual ASubcounty Subcounty { get; set; }
        public virtual ICollection<AVillage> AVillage { get; set; }
    }
}
