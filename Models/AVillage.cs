﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AVillage
    {
        public AVillage()
        {
            CoreCommunityGroupSummary = new HashSet<CoreCommunityGroupSummary>();
            CoreCrcweeklySummary = new HashSet<CoreCrcweeklySummary>();
            CoreMiycanmonthlySummary = new HashSet<CoreMiycanmonthlySummary>();
        }

        public int VillageId { get; set; }
        public string VillageName { get; set; }
        public int? ParishId { get; set; }
        public int? VillageMinistryCode { get; set; }

        public virtual AParish Parish { get; set; }
        public virtual ICollection<CoreCommunityGroupSummary> CoreCommunityGroupSummary { get; set; }
        public virtual ICollection<CoreCrcweeklySummary> CoreCrcweeklySummary { get; set; }
        public virtual ICollection<CoreMiycanmonthlySummary> CoreMiycanmonthlySummary { get; set; }
    }
}
