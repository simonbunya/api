﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ARegion
    {
        public string LocRegion { get; set; }
        public string LocDistrict { get; set; }
        public string LocSubcounty { get; set; }
        public string LocParish { get; set; }
        public string Loc { get; set; }
    }
}
