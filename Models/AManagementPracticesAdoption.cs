﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AManagementPracticesAdoption
    {
        public AManagementPracticesAdoption()
        {
            CoreCommunityGroupSummary = new HashSet<CoreCommunityGroupSummary>();
        }

        public int ManagementId { get; set; }
        public string ManagementName { get; set; }

        public virtual ICollection<CoreCommunityGroupSummary> CoreCommunityGroupSummary { get; set; }
    }
}
