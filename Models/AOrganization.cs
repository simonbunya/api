﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AOrganization
    {
        public int OrganizationId { get; set; }
        public string Organization { get; set; }
    }
}
