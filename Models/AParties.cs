﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class AParties
    {
        public AParties()
        {
            CoreCommunityGroupSummary = new HashSet<CoreCommunityGroupSummary>();
        }

        public int PartyId { get; set; }
        public string PartyName { get; set; }

        public virtual ICollection<CoreCommunityGroupSummary> CoreCommunityGroupSummary { get; set; }
    }
}
