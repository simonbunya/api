﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using RestSharp;
//using RestSharp.Authenticators;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Mvc;



using System.Data;

using System.Collections;
using System.Linq.Expressions;


using System.Reflection;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using SQL = System.Data;

using System.Configuration;

using System.Net.Sockets;
using System.Globalization;
//using System.Web;
namespace USAID_ICAN.Models
{
    public class API_Service
    {
        string userName = "ican";
        string password = "abt@1can";
        private static string Uri = String.Format("https://api.ona.io/api/v1/data/");
        public string Public_Uri = Uri;

        public List<McareGroupsData> mcgData { get; set; }
        //Ona1Chilligrps Table
        public async Task<List<ChilliData>> GetRecords()
        {
            try
            {
                List<ChilliData> item = new List<ChilliData>();
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress= new Uri(Uri);
                    client.DefaultRequestHeaders.Authorization =
                   new AuthenticationHeaderValue(
                       "Basic",
                       Convert.ToBase64String(
                           System.Text.Encoding.ASCII.GetBytes(
                               string.Format("{0}:{1}", userName, password))));

                    client.DefaultRequestHeaders.Add("ContentType", "application/json");

                    var response = await client.GetAsync("544627");
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        item = JsonConvert.DeserializeObject<List<ChilliData>>(content);
                    }
                }
                return item;
            }
            catch { throw; }
        }

        //Ona1CRC Table
        public async Task<List<CrcData>> GetCRCRecords()
        {
            try
            {
                List<CrcData> item = new List<CrcData>();
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Uri);
                    client.DefaultRequestHeaders.Authorization =
                   new AuthenticationHeaderValue(
                       "Basic",
                       Convert.ToBase64String(
                           System.Text.Encoding.ASCII.GetBytes(
                               string.Format("{0}:{1}", userName, password))));

                    client.DefaultRequestHeaders.Add("ContentType", "application/json");

                    var response = await client.GetAsync("488217");
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        item = JsonConvert.DeserializeObject<List<CrcData>>(content);
                    }
                }
                return item;
            }
            catch { throw; }
        }

        //Ona1Lgroups Table
        public async Task<List<LgroupsData>> GetLgroupsRecords()
        {
            try
            {
                List<LgroupsData> item = new List<LgroupsData>();
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Uri);
                    client.DefaultRequestHeaders.Authorization =
                   new AuthenticationHeaderValue(
                       "Basic",
                       Convert.ToBase64String(
                           System.Text.Encoding.ASCII.GetBytes(
                               string.Format("{0}:{1}", userName, password))));

                    client.DefaultRequestHeaders.Add("ContentType", "application/json");

                    var response = await client.GetAsync("487256");
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        item = JsonConvert.DeserializeObject<List<LgroupsData>>(content);
                    }
                }
                return item;
            }
            catch { throw; }
        }

        //Ona1McareGroups Table
        public async Task<List<McareGroupsData>> GetMCareGroupsRecords()
        {
            try
            {
                List<McareGroupsData> item = new List<McareGroupsData>();
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Uri);
                    client.DefaultRequestHeaders.Authorization =
                   new AuthenticationHeaderValue(
                       "Basic",
                       Convert.ToBase64String(
                           System.Text.Encoding.ASCII.GetBytes(
                               string.Format("{0}:{1}", userName, password))));

                    client.DefaultRequestHeaders.Add("ContentType", "application/json");

                    var response = await client.GetAsync("489367");
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        item = JsonConvert.DeserializeObject<List<McareGroupsData>>(content);
                    }
                }
                return item;
            }
            catch { throw; }
        }

        //### public IEnumerable<McareGroupsData> McareGroups { get; set; }


        //[HttpPost]
        //public ActionResult ViewPostedOVCData(McareGroupsData oVCUpload)
        
        public IEnumerable<McareGroupsData> GetMCareGroupsRecordss()
        {
          
            //Load the data from the API
            var model = new object[0];
            // var targeturl = "http://ovcmis.mglsd.go.ug/ovcmis_apiv3/index.php/cm_register/ovc_details/?financialYear=2018/2019&reportingPeriod=Oct%20-%20Dec&currentPage=1";
            var targeturl = string.Format("https://api.ona.io/api/v1/data/489367");
            //Jan - Mar
            HttpMessageHandler handler = new HttpClientHandler()
            {
            };

            var httpClient = new HttpClient(handler)
            {
                BaseAddress = new Uri(targeturl),
                Timeout = new TimeSpan(0, 2, 0)
            };

            List<McareGroupsData> careGroups = new List<McareGroupsData>();
            httpClient.DefaultRequestHeaders.Add("ContentType", "application/json");

            //This is the key section you were missing    
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes("ican:abt@1can");
            string val = System.Convert.ToBase64String(plainTextBytes);
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + val);

            var method = new HttpMethod("GET");
            List<string> myList = new List<string>();

            JArray jArray = new JArray();
            HttpResponseMessage response = httpClient.GetAsync(targeturl).Result;
            if (response.ReasonPhrase == "Not Found")
            {
                //TempData["Message"] = string.Format("No records found for  {0}-{1}", reportingPeriod.Replace("%20", ""), financialYear.ToString()); ;
            }
            else
            {
                string content = string.Empty;
                
                using (StreamReader stream = new StreamReader(response.Content.ReadAsStreamAsync().Result, true))
                {
                    content = stream.ReadToEnd(); 
                }

                
                jArray = JArray.Parse(content);
                // Strip off the [] from _tags and Notes
                
                foreach (JObject j in jArray)
                {
                    var cg = new McareGroupsData();
                    // cg.McareGroupsId =Convert.ToInt32( j.GetValue("McareGroupsId").ToString());
                    if (j.GetValue("district") != null)
                    {
                        cg.district = j.GetValue("district").ToString();
                    }
                    if (j.GetValue("groupid") != null)
                    {

                        cg.groupid = j.GetValue("groupid").ToString();
                    }
                    if (j.GetValue("groupname") != null)
                    {
                        cg.groupname = j.GetValue("groupname").ToString();
                    }
                    if (j.GetValue("latitude") != null)
                    {
                        cg.latitude = j.GetValue("latitude").ToString();
                    }
                    if (j.GetValue("subcounty") != null)
                    {
                        cg.subcounty = j.GetValue("subcounty").ToString();
                    }
                    if (j.GetValue("village") != null)
                    {
                        cg.village = j.GetValue("village").ToString();
                    }
                    string dvalue = string.Empty;
                    double dnumber;
                    bool success = false;
                    //cg._latitude_altitude = j.GetValue("_latitude_altitude").ToString();
                    if (j.GetValue("_latitude_latitude")!= null)
                    {
                        dvalue = j.GetValue("_latitude_latitude").ToString();
                        success = double.TryParse(dvalue, out dnumber);
                        if (success)
                        {
                            cg._latitude_latitude = dnumber;// Convert.ToDouble(j.GetValue("_latitude_latitude").ToString());
                        }
                    }


                    if (j.GetValue("_latitude_longitude") != null)
                    {
                        cg._latitude_longitude = j.GetValue("_latitude_longitude").ToString();

                    }
                    if (j.GetValue("_latitude_precision") != null)
                    {
                        cg._latitude_precision = j.GetValue("_latitude_precision").ToString();
                    }

                    if (j.GetValue("longitude") != null)
                    {
                        cg.longitude = j.GetValue("longitude").ToString();
                    }
                    if (j.GetValue("_longitude_latitude")!= null)
                    {
                        dvalue = j.GetValue("_longitude_latitude").ToString();
                        success = double.TryParse(dvalue, out dnumber);
                        if (success)
                        {
                            cg._longitude_latitude = dnumber;// Convert.ToDouble(j.GetValue("_longitude_latitude").ToString());

                        }
                    }
                    if (j.GetValue("_longitude_longitude") != null)
                    {
                        cg._longitude_longitude = j.GetValue("_longitude_longitude").ToString();
                    }
                    if (j.GetValue("_longitude_altitude") != null)
                    {
                        cg._longitude_altitude = j.GetValue("_longitude_altitude").ToString();
                    }
                    if (j.GetValue("_id")!= null)
                    {
                        dvalue = j.GetValue("_id").ToString();
                        success = double.TryParse(dvalue, out dnumber);
                        if (success)
                        {
                            cg._id = dnumber;// Convert.ToDouble(j.GetValue("_id").ToString());
                        }
                    }
                    if (j.GetValue("_longitude_precision") != null)
                    {
                        cg._longitude_precision = j.GetValue("_longitude_precision").ToString();
                    }
                    if (j.GetValue("meta/instanceID") != null)
                    {
                        cg.MetaInstanceId = j.GetValue("meta/instanceID").ToString();
                    }
                    if (j.GetValue("_uuid") != null)
                    {
                        cg._uuid = j.GetValue("_uuid").ToString();
                    }

                    if (j.GetValue("_submission_time")!= null)
                    {
                        CultureInfo enUS = new CultureInfo("en-US");
                        string dateString;
                        DateTime dateValue;
                        // Use custom formats with M and MM.
                        dateString = j.GetValue("_submission_time").ToString();
                        if (DateTime.TryParseExact(dateString, "M/dd/yyyy hh:mm", enUS,
                                                   DateTimeStyles.None, out dateValue))
                        {
                            cg._submission_time = dateValue;// Convert.ToDateTime(j.GetValue("_submission_time").ToString());
                        }
                    }

                    if (j.GetValue("_index")!= null)
                    {
                        dvalue = j.GetValue("_index").ToString();
                        success = double.TryParse(dvalue, out dnumber);
                        if (success)
                        {
                            cg._index = dnumber;// Convert.ToDouble(j.GetValue("_index").ToString());
                        }
                    }
                    if (j.GetValue("_parent_table_name") != null)
                    {
                        cg._parent_table_name = j.GetValue("_parent_table_name").ToString();

                    }

                    if (j.GetValue("_parent_index")!= null)
                    {
                        dvalue = j.GetValue("_parent_index").ToString();
                        success = double.TryParse(dvalue, out dnumber);
                        if (success)
                        {
                            cg._parent_index = dnumber;// Convert.ToDouble(j.GetValue("_parent_index").ToString());
                        }
                    }
                    if (j.GetValue("_tags") != null)
                    {
                        cg._tags = j.GetValue("_tags").ToString();
                    }
                    if (j.GetValue("_notes") != null)
                    {
                        cg._notes = j.GetValue("_notes").ToString();
                    }
                    if (j.GetValue("_version") != null)
                    {
                        cg._version = j.GetValue("_version").ToString();
                    }
                    if (j.GetValue("_duration") != null)
                    {
                        cg._duration = j.GetValue("_duration").ToString();
                    }
                    if (j.GetValue("_submitted_by") != null)
                    {
                        cg._submitted_by = j.GetValue("_submitted_by").ToString();
                    }
                    if (j.GetValue("_xform_id")!= null)
                    {
                        dvalue = j.GetValue("_xform_id").ToString();
                        success = double.TryParse(dvalue, out dnumber);
                        if (success)
                        {
                            cg._xform_id = dnumber;// Convert.ToDouble(j.GetValue("_xform_id").ToString());
                        }
                    }
                    careGroups.Add(cg);
                }
               // careGroups = JsonConvert.DeserializeObject<List<McareGroupsData>>(content);

                //content = content.TrimStart(new char[] { '[' }).TrimEnd(new char[] { ']' });
                //JObject ovcmis = JObject.Parse(content); 
            }

            this.mcgData= careGroups;

            return mcgData;
        }




        //Ona1samplegrps Table
        public async Task<List<Samplegrps>> GetsamplegrpsRecords()
        {
            try
            {
                List<Samplegrps> item = new List<Samplegrps>();
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Uri);
                    client.DefaultRequestHeaders.Authorization =
                   new AuthenticationHeaderValue(
                       "Basic",
                       Convert.ToBase64String(
                           System.Text.Encoding.ASCII.GetBytes(
                               string.Format("{0}:{1}", userName, password))));

                    client.DefaultRequestHeaders.Add("ContentType", "application/json");

                    var response = await client.GetAsync("537317");
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        item = JsonConvert.DeserializeObject<List<Samplegrps>>(content);
                    }
                }
                return item;
            }
            catch { throw; }
        }

    }

    public class ChilliData
    {
        public int ChilliGrpId { get; set; }
        public string district { get; set; }
        public string groupid { get; set; }
        public string groupname { get; set; }
        public string subcounty { get; set; }
        public double? Male_Youth { get; set; }
        public double? Male_Adults { get; set; }
        public double? Female_Youth { get; set; }
        public double? Female_Adults { get; set; }
        public double? Total_number_of_participants { get; set; }
        public string parish { get; set; }
        public string village { get; set; }
        //public string MetaInstanceId { get; set; }
        public string meta_instanceID { get; set; }
        //meta/instanceID
        public double? _id { get; set; }
        public string _uuid { get; set; }
        public DateTime? _submission_time { get; set; }
        public double? _index { get; set; }
        public string _parent_table_name { get; set; }
        public double? _parent_index { get; set; }      
        public string _version { get; set; }
        public string _duration { get; set; }
        public string _submitted_by { get; set; }
        public double? _xform_id { get; set; }
        public List<object> _notes { get; set; }
        public List<object> _tags { get; set; }
        public List<object> _attachments { get; set; }
        public List<object> _geolocation { get; set; }
    }

    public class CrcData
    {
        public int Crcid { get; set; }
        public string district { get; set; }
        public string subcounty { get; set; }
        public string parish { get; set; }
        public string village { get; set; }
        public string schoolname { get; set; }
        public string schoolid { get; set; }
        public string school { get; set; }
        public double? _latitude_latitude { get; set; }
        public string clubname { get; set; }
        public string latitude { get; set; }
        public string _latitude_longitude { get; set; }
        public string _latitude_altitude { get; set; }
        public string _latitude_precision { get; set; }
        public string longitude { get; set; }
        public double? _longitude_latitude { get; set; }
        public string _longitude_longitude { get; set; }
        public string _longitude_altitude { get; set; }
        public double? _id { get; set; }
        public string _longitude_precision { get; set; }
        //public string MetaInstanceId { get; set; }
        public string meta_instanceID { get; set; }
        public string _uuid { get; set; }
        public DateTime? _submission_time { get; set; }
        public double? _index { get; set; }
        public string _parent_table_name { get; set; }
        public double? _parent_index { get; set; }        
        public string _version { get; set; }
        public string _duration { get; set; }
        public string _submitted_by { get; set; }
        public double? _xform_id { get; set; }
        public List<object> _notes { get; set; }
        public List<object> _tags { get; set; }
        public List<object> _attachments { get; set; }
        public List<object> _geolocation { get; set; }
    }

    public class LgroupsData
    {
        public int LgroupsId { get; set; }
        public string district { get; set; }
        public string groupid { get; set; }
        public string groupname { get; set; }
        public string subcounty { get; set; }
        public string parish { get; set; }
        public string village { get; set; }
        public string latitude { get; set; }
        public string _latitude_altitude { get; set; }
        public double? _latitude_latitude { get; set; }
        public string _latitude_longitude { get; set; }
        public string _latitude_precision { get; set; }
        public string longitude { get; set; }
        public double? _longitude_latitude { get; set; }
        public string _longitude_longitude { get; set; }
        public string _longitude_altitude { get; set; }
        public string _longitude_precision { get; set; }
        public double? maleyouth { get; set; }
        public double? femaleyouth { get; set; }
        public double? maleadults { get; set; }
        public double? femaleadults { get; set; }
        public double? total { get; set; }
        public string MetaInstanceId { get; set; }
        //[meta/instanceID]
        public double? _id { get; set; }
        public string _uuid { get; set; }
        public DateTime? _submission_time { get; set; }
        public double? _index { get; set; }
        public string _parent_table_name { get; set; }
        public double? _parent_index { get; set; }
       
        public string _version { get; set; }
        public string _duration { get; set; }
        public string _submitted_by { get; set; }
        public double? _xform_id { get; set; }

        public List<object> _notes { get; set; }
        public List<object> _tags { get; set; }
        public List<object> _attachments { get; set; }
        public List<object> _geolocation { get; set; }
    }

    // Ona1McareGroups
    public class McareGroupsData
    {
        public int McareGroupsId { get; set; }
        public string district { get; set; }
        public string groupid { get; set; }
        public string groupname { get; set; }
        public string latitude { get; set; }
        public string subcounty { get; set; }
        public string village { get; set; }
        public string _latitude_altitude { get; set; }
        public double? _latitude_latitude { get; set; }
        public string _latitude_longitude { get; set; }
        public string _latitude_precision { get; set; }
        public string longitude { get; set; }
        public double? _longitude_latitude { get; set; }
        public string _longitude_longitude { get; set; }
        public string _longitude_altitude { get; set; }
        public double? _id { get; set; }
        public string _longitude_precision { get; set; }
        public string MetaInstanceId { get; set; }
        //[meta/instanceID]
        public string _uuid { get; set; }
        public DateTime? _submission_time { get; set; }
        public double? _index { get; set; }
        public string _parent_table_name { get; set; }
        public double? _parent_index { get; set; }
        public string _tags { get; set; }
        public string _notes { get; set; }
        public string _version { get; set; }
        public string _duration { get; set; }
        public string _submitted_by { get; set; }
        public double? _xform_id { get; set; }
    }

    // Ona1samplegrps
    public class Samplegrps
    {
        public int SmpleGrrpsId { get; set; }
        public string district { get; set; }
        public string groupid { get; set; }
        public string groupname { get; set; }
        public string subcounty { get; set; }
        public string parish { get; set; }
        public string village { get; set; }
        public double? latitude { get; set; }
        public double? _id { get; set; }
        public double? longitude { get; set; }
        public string MetaInstanceId { get; set; }
        //[meta/instanceID]
        public string _uuid { get; set; }
        public DateTime? _submission_time { get; set; }
        public double? _index { get; set; }
        public string _parent_table_name { get; set; }
        public double? _parent_index { get; set; }
        public string _tags { get; set; }
        public string _notes { get; set; }
        public string _version { get; set; }
        public string _duration { get; set; }
        public string _submitted_by { get; set; }
        public double? _xform_id { get; set; }
    }

}