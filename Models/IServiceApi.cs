﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_System.Models
{
    public interface IServiceApi
    {
        //    Task SaveChilliData();
        //    Task SaveCRCData();
        //    Task SaveLgroupsData();
        //    Task SaveMCareGroupsData();
        //    Task SaveSampleGroupsData();

        void SaveChilliData();
        void SaveCRCData();
        void SaveLgroupsData();
        void SaveMCareGroupsData();
        void SaveSampleGroupsData();
        void SaveAGYW2020Data();
        //void SaveBSPSurveyData();
        void SaveBSPSurveyfinalData();
        void SaveCGAssessmentData();
        void SaveHHProfileData();
        void SavemcgData();
        void Savevetrac2Data();
        void SavecrcweeklysummData();
        void SavemiycanmonSumData();
        void SavecommgrpsumData();
        void SaveukuregData();
        void SavebspsurrevData();
        void SaveUpdlivgroupData();
        void SavemiycangroupData();
        void SaveEreProfilingData();
        void SaverefNoteData();
        void SaveRMS1Data();
        void SavecellprofilingData();
        void SaveInstnMappgData();
        void SavePostTestData();
        void SaveIBS2020Data();


    }
}

