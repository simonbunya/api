﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ABusinessOpportunityMeeting
    {
        public ABusinessOpportunityMeeting()
        {
            CoreCommunityGroupSummary = new HashSet<CoreCommunityGroupSummary>();
        }

        public int BomId { get; set; }
        public string BomName { get; set; }

        public virtual ICollection<CoreCommunityGroupSummary> CoreCommunityGroupSummary { get; set; }
    }
}
