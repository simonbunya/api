﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewOnaCgassessmentsuppfarvslaPdtandsavings
    {
        public double Id { get; set; }
        public string Fieldteam { get; set; }
        public string QnIntroStart { get; set; }
        public string QnIntroEnd { get; set; }
        public string QnIntroLocRegion { get; set; }
        public string QnIntroLocDistrict { get; set; }
        public string QnIntroLocSubcounty { get; set; }
        public string QnIntroLocParish { get; set; }
        public string QnAA5 { get; set; }
        public string QnGvScheme { get; set; }
        public int? QnGvGrptype { get; set; }
        public string GroupTypeName { get; set; }
        public double? QnIntroMaleadults { get; set; }
        public double? QnIntroFemaleadults { get; set; }
    }
}
