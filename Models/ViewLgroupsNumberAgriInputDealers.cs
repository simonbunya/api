﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewLgroupsNumberAgriInputDealers
    {
        public double? Id { get; set; }
        public string Groupid { get; set; }
        public string LocationGroupid { get; set; }
        public string District { get; set; }
        public string GroupName { get; set; }
        public string Subcounty { get; set; }
        public string Parish { get; set; }
        public string Village { get; set; }
        public string Lm { get; set; }
        public int? Year { get; set; }
        public string Month { get; set; }
    }
}
