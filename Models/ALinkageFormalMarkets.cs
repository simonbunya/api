﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ALinkageFormalMarkets
    {
        public ALinkageFormalMarkets()
        {
            CoreCommunityGroupSummary = new HashSet<CoreCommunityGroupSummary>();
        }

        public int MarketId { get; set; }
        public string MarketName { get; set; }

        public virtual ICollection<CoreCommunityGroupSummary> CoreCommunityGroupSummary { get; set; }
    }
}
