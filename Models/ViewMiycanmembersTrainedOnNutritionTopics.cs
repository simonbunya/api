﻿using System;
using System.Collections.Generic;

namespace API_System.Models
{
    public partial class ViewMiycanmembersTrainedOnNutritionTopics
    {
        public double Id { get; set; }
        public string District { get; set; }
        public string Subcounty { get; set; }
        public string Village { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string Mod { get; set; }
    }
}
